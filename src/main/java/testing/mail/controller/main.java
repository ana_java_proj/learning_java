package testing.mail.controller;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.StringWriter;


@RestController
public class main {
    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @RequestMapping("test")
    public String test() {
        VelocityEngine vlcEngine = new VelocityEngine();
        vlcEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        vlcEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        vlcEngine.init();
        Template t = vlcEngine.getTemplate("templates/testTemplateJapanese.txt", "UTF-8");
        VelocityContext context = new VelocityContext();
        context.put("name","ttuan");
        context.put("testText","株式会社かぶしきがいしゃワサビ　山やま田だ 武たけし様さま");
        StringWriter writer = new StringWriter();
        t.merge(context, writer);
        return writer.toString();
    }

    @RequestMapping("send")
    public String sendMail() {

    }
}
