package testing.mail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MailUtilServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MailUtilServicesApplication.class, args);
	}
}